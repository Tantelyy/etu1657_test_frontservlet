<%-- 
    Document   : form.jsp
    Created on : 14 nov. 2022, 08:52:18
    Author     : tantely
--%>
<%@page import="modele.Dept" %>
<%@page import="java.util.Vector" %>
<% Vector listeDept = new Dept().getAll(); %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Insertion employe</title>
    </head>
    <body>
        <h1>Insertion employe</h1>
        <form method="POST" action="emp-save.do">
            <p>Nom: <input type="text" name="nom" /></p>
            <p>Prenom: <input type="text" name="prenom" /></p>
            <p>Nom departement: <select name="idDept">
                    <% for (int i = 0; i < listeDept.size(); i++) { %>
                    <% Dept dept = (Dept)listeDept.elementAt(i); %>
                        <option value="<%= dept.getIdDept() %>"><%= dept.getNomDept() %></option>
                    <% } %>
            </select></p>
            <input type="submit" value="Inserer" />
        </form>
        <br/>
        <a href="index.jsp"><button>Retour</button></a>
    </body>
</html>
