<%-- 
    Document   : liste.jsp
    Created on : 10 nov. 2022, 08:58:59
    Author     : tantely
--%>

<%@page import="java.util.Vector" %>
<%@page import="modele.Emp" %>
<%@page import="modele.Dept" %>
<% Vector liste = (Vector)request.getAttribute("liste"); %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Liste des employes</h1>
        <table border="1">
            <thead>
                <tr>
                    <th>Nom</th>
                    <th>Prenom</th>
                    <th>Departement</th>
                </tr>
            </thead>
            <tbody>
                <% for (int i = 0; i < liste.size(); i++) { %>
                    <% Emp emp = (Emp)liste.elementAt(i); %>
                    <tr>
                        <td><%= emp.getNom() %></td>
                        <td><%= emp.getPrenom() %></td>
                        <td><%= emp.getIdDept().getNomDept() %></td>
                    </tr>
                <% } %>
            </tbody>
        </table>
            
        <br />
        <a href="index.jsp"><button>Retour</button></a>
    </body>
</html>
