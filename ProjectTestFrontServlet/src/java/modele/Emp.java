/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modele;

import annotations.AnnotationColonne;
import annotations.AnnotationTable;
import annotations.UrlAnnotation;
import dao.Connexion;
import dao.GenericDAO;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import tools.ModelView;

/**
 *
 * @author tantely
 */

@AnnotationTable(table = "emp")
public class Emp {
    @AnnotationColonne(column="empno", pk = true)
    private Integer id;
    
    @AnnotationColonne(column = "nom")
    private String nom;
    
    @AnnotationColonne(column = "prenom")
    private String prenom;
    
    @AnnotationColonne(column = "deptno")
    private Dept idDept;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Dept getIdDept() {
        return idDept;
    }

    public void setIdDept(Dept idDept) {
        this.idDept = idDept;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    

    @UrlAnnotation(url = "emp-list")
    public ModelView list() throws SQLException {
        ModelView mv = new ModelView();
        mv.setUrl("liste.jsp");
        Connexion connex = new Connexion("generic", "generic", "jdbc:postgresql://localhost:5432/generic", "org.postgresql.Driver");
        Connection connexion = null;
        try {
            connexion = connex.getConn();
            Vector empListe = new GenericDAO().select(this, connexion, null);
            System.out.println(empListe.size() + " = taille");

            HashMap<String, Object> hash = new HashMap<>();
            hash.put("liste", empListe);
            
            mv.setData(hash);
        } catch (SQLException | ClassNotFoundException | NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | InstantiationException | NoSuchFieldException ex) {
            Logger.getLogger(Emp.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connexion != null) connexion.close();
        }

        return mv;
    }

    @UrlAnnotation(url = "emp-save")
    public ModelView save() throws SQLException {
        Connexion connex = new Connexion("generic", "generic", "jdbc:postgresql://localhost:5432/generic", "org.postgresql.Driver");
        Connection connexion = null;
        ModelView mv = new ModelView();
        mv.setUrl("index.jsp");
        try {
            connexion = connex.getConn();
            new GenericDAO().save(this, connexion);

            HashMap<String, Object> hash = new HashMap<>();
            
            mv.setData(hash);
        } catch (Exception ex) {
            Logger.getLogger(Emp.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (connexion != null) connexion.close();
        }
        
        return mv;
    }

}
