/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modele;

import annotations.AnnotationColonne;
import annotations.AnnotationTable;
import dao.Connexion;
import dao.GenericDAO;
import java.sql.Connection;
import java.util.Vector;

/**
 *
 * @author tantely
 */
@AnnotationTable(table = "dept")
public class Dept {
    @AnnotationColonne(column = "id", pk = true)
    private Integer idDept;
    
    @AnnotationColonne(column = "dname")
    private String nomDept;

    public Integer getIdDept() {
        return idDept;
    }

    public void setIdDept(Integer idDept) {
        this.idDept = idDept;
    }

    public String getNomDept() {
        return nomDept;
    }

    public void setNomDept(String nomDept) {
        this.nomDept = nomDept;
    }
    
    public Vector getAll() throws Exception {
        Connexion conn = new Connexion("generic", "generic", "jdbc:postgresql://localhost:5432/generic", "org.postgresql.Driver");
        Connection connexion = conn.getConn();
        Vector response = new GenericDAO().select(this, connexion, null);
        if (connexion != null) connexion.close();
        return response;
    }

}
