CREATE USER generic WITH PASSWORD 'generic';
CREATE DATABASE generic;
ALTER DATABASE generic OWNER TO generic;

CREATE TABLE Emp (
    empno SERIAL PRIMARY KEY,
    nom VARCHAR(50) NOT NULL,
    prenom VARCHAR(50) NOT NULL,
    deptno INTEGER NOT NULL
);

CREATE TABLE Dept (
    id SERIAL PRIMARY KEY,
    dname VARCHAR(30) NOT NULL
);

ALTER TABLE Emp ADD FOREIGN KEY (deptno) REFERENCES Dept (id);

INSERT INTO Dept VALUES (default, 'Dept 1');
INSERT INTO Dept VALUES (default, 'Dept 2');

INSERT INTO Emp VALUES (default, 'Rakoto', 'Georges', 1);
INSERT INTO Emp VALUES (default, 'Rakotoarisoa', 'Annie', 1);
INSERT INTO Emp VALUES (default, 'Rabearilala', 'Tafita', 2);